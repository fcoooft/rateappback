package com.Hackaton.Hackaton2.models;

public class ProductModel {

    private String id;
    private String title;
    private String desc;
    private String team;
    private String image;
    private String url;
    private int rate;

    public ProductModel(){

    }

    public ProductModel(String id, String title, String desc, String team, String image, String url, int rate) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.team = team;
        this.image = image;
        this.url = url;
        this.rate = rate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
