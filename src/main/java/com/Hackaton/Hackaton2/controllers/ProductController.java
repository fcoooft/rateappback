package com.Hackaton.Hackaton2.controllers;

import com.Hackaton.Hackaton2.models.ProductModel;
import com.Hackaton.Hackaton2.services.ProductService;
import com.Hackaton.Hackaton2.services.ProductServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

@CrossOrigin (origins = "*" , methods  = {RequestMethod.GET, RequestMethod.PATCH})

@RestController
@RequestMapping("/rateAppAPI/v1")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.productService.findAll(),
                HttpStatus.OK
        );
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getproductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("la id del producto a buscar es " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PatchMapping("/products/{id}")
    public ResponseEntity<ProductModel> addRatio(@RequestBody ProductModel ratio){
        System.out.println("addRatio");
        System.out.println("la id del producto a añadir es " + ratio.getId());
        System.out.println("La id del producto de la compra a añadir es " + ratio.getDesc());
        System.out.println("El equipo del producto es " + ratio.getTeam());

        ProductServiceResponse productServiceResponse = this.productService.addRatio(ratio);

        return new ResponseEntity<>(
                productServiceResponse.getRatio(),
                productServiceResponse.getResponseHttpStatusCode()
        );
    }
}
