package com.Hackaton.Hackaton2.repositories;

import com.Hackaton.Hackaton2.Hackaton2Application;
import com.Hackaton.Hackaton2.models.ProductModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProductRepository {

    public List<ProductModel> findAll(){
        System.out.println("findAll en ProductRepository");

        return Hackaton2Application.productModels;
    }

    public Optional<ProductModel> findById(String id){
        System.out.println("findById en ProductRepository");

        Optional<ProductModel> result = Optional.empty();

        for (ProductModel productInList : Hackaton2Application.productModels) {
            if(productInList.getId().equals(id)) {
                System.out.println("Producto encontrado" + productInList.getId());
                result = Optional.of(productInList);
            }
        }
        return result;
    }

    public ProductModel save(ProductModel purchase){
        System.out.println("Save en UserRepository");

        Hackaton2Application.productModels.add(purchase);

        return purchase;
    }

}
