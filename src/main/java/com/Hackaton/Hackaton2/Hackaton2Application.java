package com.Hackaton.Hackaton2;

import com.Hackaton.Hackaton2.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class Hackaton2Application {

	public static ArrayList<ProductModel> productModels;


	public static void main(String[] args) {
		SpringApplication.run(Hackaton2Application.class, args);


		Hackaton2Application.productModels = Hackaton2Application.getTestData();
	}

	private static ArrayList<ProductModel> getTestData(){


		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1",
						"Aplicacion1"
						,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius mauris mi, a efficitur orci mollis cursus.\n"
						, "Equipo1"
						, "https://via.placeholder.com/500x300"
						,"https://www.google.com"
						, 0
				)
		);

		productModels.add(
				new ProductModel(
						"2",
						"Aplicacion2"
						,"Pellentesque accumsan nisl vel erat rutrum, ut sagittis est porttitor. Sed fermentum nunc id leo molestie cursus.\n"
						, "Equipo2"
						, "https://via.placeholder.com/500x300"
						, "https://facebook.com"
						, 0
				)
		);
		productModels.add(
				new ProductModel(
						"3"
						, "Aplicacion3"
						,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius mauris mi, a efficitur orci mollis cursus.\n"
						, "Equipo3"
						, "https://via.placeholder.com/500x300"
						, "https://instagram.com"
						, 0
				)
		);

		productModels.add(
				new ProductModel(
						"4"
						,"RateApp"
						,"Pellentesque accumsan nisl vel erat rutrum, ut sagittis est porttitor. Sed fermentum nunc id leo molestie cursus.\n"
						, "Equipo4"
						, "https://via.placeholder.com/500x300"
						, "https://guarded-waters-66204.herokuapp.com/rateAppAPI/v1/products"
						, 0
				)
		);

		productModels.add(
				new ProductModel(
						"5"
						,"Aplicacion5"
						,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius mauris mi, a efficitur orci mollis cursus.\n"
						, "Equipo5"
						, "https://via.placeholder.com/500x300"
						, "https://spotify.com"
						, 0
				)
		);

		productModels.add(
				new ProductModel(
						"6"
						,"Aplicacion6"
						,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius mauris mi, a efficitur orci mollis cursus.\n"
						, "Equipo6"
						, "https://via.placeholder.com/500x300"
						, "https://youtube.com"
						, 0
				)
		);

		productModels.add(
				new ProductModel(
						"7"
						,"Aplicacion7"
						,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius mauris mi, a efficitur orci mollis cursus.\n"
						, "Equipo7"
						, "https://via.placeholder.com/500x300"
						, "https://linkedin.com"
						, 0
				)
		);

		productModels.add(
				new ProductModel(
						"8"
						,"Aplicacion8"
						,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius mauris mi, a efficitur orci mollis cursus.\n"
						, "Equipo8"
						, "https://via.placeholder.com/500x300"
						, "https://github.com"
						, 0
				)
		);

		return productModels;
	}

	private static ArrayList<ProductModel> getTestDataProducts(){

		ArrayList<ProductModel> productModels = new ArrayList<>();
		return productModels;
	}

}
