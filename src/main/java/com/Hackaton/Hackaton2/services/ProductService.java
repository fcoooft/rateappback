package com.Hackaton.Hackaton2.services;

import com.Hackaton.Hackaton2.Hackaton2Application;
import com.Hackaton.Hackaton2.models.ProductModel;
import com.Hackaton.Hackaton2.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductService productService;


    public List<ProductModel> findAll(){
        System.out.println("findAll en ProductService");

        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id){
        System.out.println("findById en ProductService");

        return this.productRepository.findById(id);
    }

    public ProductServiceResponse addRatio(ProductModel ratio){
        System.out.println("add en ProductService");

        ProductServiceResponse result = new ProductServiceResponse();

        result.setRatio(ratio);

        Optional<ProductModel> productToUpdate =  this.findById(ratio.getId());

        if (productToUpdate.isPresent() == true){
            System.out.println("Producto para actualizar encontrado");

            ProductModel productFromList = productToUpdate.get();

            for (ProductModel purchaseItem : Hackaton2Application.productModels){
                System.out.println("Inicializar for");
                System.out.println(this.productService.findById(ratio.getId()).get().getRate());
                System.out.println(ratio.getRate());

                productFromList.setRate(ratio.getRate() + this.productService.findById(purchaseItem.getId()).get().getRate());
                System.out.println("Se han añadido los ratios nuevos");
                result.setResponseHttpStatusCode(HttpStatus.OK);
                return result;

            }

        }else {
            result.setResponseHttpStatusCode(HttpStatus.NOT_FOUND);
            System.out.println("Producto no encontrado");

        }

        return result;

        }
}
