package com.Hackaton.Hackaton2.services;

import com.Hackaton.Hackaton2.models.ProductModel;
import org.springframework.http.HttpStatus;

public class ProductServiceResponse {

    private String msg;
    private ProductModel ratio;
    private HttpStatus responseHttpStatusCode;

    public ProductServiceResponse(){

    }

    public ProductServiceResponse(String msg, ProductModel ratio, HttpStatus responseHttpStatusCode) {
        this.msg = msg;
        this.ratio = ratio;
        this.responseHttpStatusCode = responseHttpStatusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ProductModel getRatio() {
        return ratio;
    }

    public void setRatio(ProductModel ratio) {
        this.ratio = ratio;
    }

    public HttpStatus getResponseHttpStatusCode() {
        return responseHttpStatusCode;
    }

    public void setResponseHttpStatusCode(HttpStatus responseHttpStatusCode) {
        this.responseHttpStatusCode = responseHttpStatusCode;
    }
}
